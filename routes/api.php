<?php

/** rota para o aluno acessar o sistema, se sucesso, retorna o token de acesso **/
Route::post('login-aluno', 'AlunoController@getLoginAluno');

/** rota para o professor acessar o sistema, se sucesso, retorna o token de acesso **/
Route::post('login-professor', 'ProfessorController@getLoginProfessor');

/** Rotas protegidas, só acessa essas rotas quem tem o token **/
Route::group(['middleware' => 'auth:api'], function() {

    //Route::resource('usuarios', 'UsuarioController');

    /**rotas do professor, nos metodos dessa classe tem o detalhes **/
    Route::resource('professores', 'ProfessorController');

    /** Rota para adicionar um aluno ao jogo **/
    Route::post('adicionar-aluno-jogo', 'JogoController@InscricaoDeAluno');
    /** Rota para retirar um aluno ao jogo **/
    Route::delete('retirar-aluno-jogo', 'JogoController@RemoverAlunoDoJogo');

    /** Rota para adicionar um aluno ao grupo **/
    Route::post('adicionar-aluno-grupo', 'AlunoController@AdicionarAlunoAoGrupo');
    /** Rota para retirar um aluno do grupo **/
    Route::delete('retirar-aluno-grupo', 'AlunoController@RemoverAlunoDoGrupo');

    /** Rota para adicionar presença ao aluno **/
    Route::post('adicionar-presenca-aluno', 'SessaoController@AdicionarPresencaDoAlunoNaSessao');
    /** Rota para retirar presença ao aluno **/    
    Route::delete('retirar-presenca-aluno', 'SessaoController@RetirarPresencaDoAluno');

    /** Rota para adicionar medalha ao aluno **/
    Route::post('adicionar-medalha-aluno', 'MedalhaController@AdicionarMedalhaAluno');
    /** Rota para retirar medalha ao aluno **/
    Route::delete('retirar-medalha-aluno', 'MedalhaController@RetirarMedalhaAluno');

    
    /** rota para pegar os jogos de um professor **/
    Route::get('jogos-professor/{id}', 'SessaoController@JogosDoProfessor');

    /**rotas do aluno, nos metodos dessa classe tem o detalhes **/
    Route::resource('alunos', 'AlunoController');

    /**rotas do jogo, nos metodos dessa classe tem o detalhes **/
    Route::resource('jogos', 'JogoController');

    /**rotas da sessão, nos metodos dessa classe tem o detalhes **/
    Route::resource('sessoes', 'SessaoController');

    /**rotas da medalha, nos metodos dessa classe tem o detalhes **/
    Route::resource('medalhas', 'MedalhaController');

    /**rotas do grupo, nos metodos dessa classe tem o detalhes **/
    Route::resource('grupos', 'GrupoController');

    //Route::resource('ranking', 'RankingController');
    //Route::resource('redessociais', 'RedeSocialController');

});