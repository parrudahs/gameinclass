<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class JogoValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
         	'nome' => 'required',
	    	'descricao' => 'required', 
	    	'inscricao' => 'required', 
	    	'curso' => 'required', 
	    	'semestre' => 'required', 
	    	'sessao_label'=> 'required',  
	    	'professor_id' => 'required',
	    	'pontos_individual' => 'required',
        	'pontos_grupo'  => 'required',
        	'medalhas_individual'  => 'required',
        	'medalhas_grupo'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
  			'nome' => 'required',
	    	'descricao' => 'required', 
	    	'inscricao' => 'required',
	    	'curso' => 'required', 
	    	'semestre' => 'required', 
	    	'sessao_label'=> 'required',  
	    	'professor_id' => 'required',
	    	'pontos_individual' => 'required',
        	'pontos_grupo'  => 'required',
        	'medalhas_individual'  => 'required',
        	'medalhas_grupo'  => 'required',
        ],
   ];
}
