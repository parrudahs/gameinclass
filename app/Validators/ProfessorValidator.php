<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class ProfessorValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
        	'cpf' => 'required|min:12|max:12',
        	'nome' => 'required',
        	'email' => 'required|email',
        	'sobrenome' => '',
        	'senha' => 'required|min:5',
        	'password_confirmation' => 'required|min:5|same:senha'
        ],
        ValidatorInterface::RULE_UPDATE => [
        	'cpf' => 'required|min:12|max:12',
        	'nome' => '',
        	'email' => 'required|email',
        	'sobrenome' => 'required',
        	'senha' => 'required|min:5',
        	'password_confirmation' => 'required|min:5|same:senha'
        ],
   ];
}
