<?php 

namespace App\Services;

use App\Repositories\SessaoRepository;
use App\Validators\SessaoValidator;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\DB;

class SessaoService
{

	private $repository;

	private $validator;

	public function __construct(SessaoRepository $repository, SessaoValidator $validator) 
	{
		$this->repository = $repository;
		$this->validator = $validator;
	}

	public function create(array $data) 
	{
		try{

			$this->validator->with($data)->passesOrFail();
			return $this->repository->create($data);

		}catch(ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}
	}

	public function update(array $data, $id) 
	{
		try{
			$this->validator->with($data)->passesOrFail();
			return $this->repository->update($data, $id);

		} catch (ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}	
	}


	public function AdicionarPresencaDoAlunoNaSessao(array $data)
	{
		$sessao = DB::table('sessoes_alunos')
						->where('sessao_id', '=', $data['sessao_id'])
						->where('aluno_id', '=', $data['aluno_id'])
						->count();
		if($sessao <= 0) {
			$sessao = DB::table('sessoes_alunos')
					->insert($data);
			return [
	            'error' => false,
	            'message' => 'A presença foi adicionada com sucesso ao aluno.'
        	];
		} else {
			return [
	            'error' => true,
	            'message' => 'O aluno já esta com presença'
        	];
		}
	}

	public function RetirarPresencaDoAluno(array $data)
	{
		$grupo = DB::table('sessoes_alunos')
					->where('sessao_id', '=', $data['sessao_id'])
					->where('aluno_id', '=', $data['aluno_id'])
					->delete();
		if($grupo){
			return [
	            'error' => false,
	            'message' => 'A presença foi retirada com sucesso do aluno.'
        	];
        }

        return [
	            'error' => true,
	            'message' => 'erro ao retirar o presença do aluno'
        ];
	}

}