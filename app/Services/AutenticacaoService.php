<?php
namespace App\Services;


use App\Repositories\AlunoRepository;
use App\Repositories\ProfessorRepository;
use App\Repositories\UsuarioRepository;

class AutenticacaoService
{
    private $aluno;
    private $usuario;
    private $professor;

    public function __construct(AlunoRepository $aluno, ProfessorRepository $professor, UsuarioRepository $usuario)
    {
        $this->aluno = $aluno;
        $this->professor = $professor;
        $this->usuario = $usuario;
    }

    /**
     * 
     */
    public function getLoginAluno(array $data)
    {        
        $aluno = $this->aluno->findByField('rga', $data['rga'])->first();

        if(!$aluno)
            return ['error' => true, 'message' =>  'O RGA é inválido', 'rga' => true];

        if($aluno->usuario->senha != $data['senha'])
            return ['error' => true, 'message' =>  'A senha não é válida', 'senha' => true];

        return [
            'error' => false,
            'token' => $this->usuario->find($aluno->usuario->id)->createToken('Token'.$aluno->usuario->nome)->accessToken,
            'aluno' => $aluno,
        ];
    }

    /**
     * 
     */
    public function getLoginProfessor(array $data) 
    {
        $professor  = $this->professor->findByField('cpf', $data['cpf'])->first();

        if(!$professor)
            return ['error' => true, 'message' =>  'O CPF é inválido', 'cpf' => true];

        if($professor->usuario->senha != $data['senha'])
            return ['error' => true, 'message' =>  'A senha não é válida', 'senha' => true];

        return [
            'error' => false,
            'token' => $this->usuario->find($professor->usuario->id)->createToken('Token'.$professor->usuario->nome)->accessToken,
            'professor' => $professor,
        ];
    }

}

