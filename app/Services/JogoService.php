<?php 

namespace App\Services;

use App\Repositories\JogoRepository;
use App\Repositories\RankingRepository;
use App\Validators\JogoValidator;
use \Prettus\Validator\Exceptions\ValidatorException;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\DB;

class JogoService
{

	private $repository;

	private $ranking;

	private $validator;

	public function __construct(JogoRepository $repository, JogoValidator $validator, RankingRepository $ranking) 
	{
		$this->repository = $repository;
		$this->validator = $validator;
		$this->ranking = $ranking;
	}

	/**
	 * Cadastro de Jogos com Ranking
	 * esse metodo cadastra os jogos com o ranking de cada jogo
	 */
	public function create(array $data) 
	{
		try{

			$this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_CREATE );
			$jogoCadastrado =  $this->repository->create($data);
			$data['jogo_id'] = $jogoCadastrado->id;
			if($this->ranking->create($data)){
				return [
					'error' => false,
					'message' => 'O Jogo foi cadastrado com sucesso'
				];
			} else {
				$this->repository->find($jogoCadastrado->id)->delete();
				return [
					'error' => true,
					'message' => 'Erro ao cadastrar jogos'
				];
			}
		}catch(ValidatorException $e) {
			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];
		}
	}

	/**
	 * Atualização de Jogos com Ranking
	 * esse metodo atualiza os jogos com o ranking de cada jogo
	 */
	public function update(array $data, $id) 
	{
		try{
			$this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_UPDATE );
			$rank = $this->ranking->findByField('jogo_id', $id)->first();

			if($this->ranking->update($data, $rank->id)){
				return $this->repository->update($data, $id);
			}					

		} catch (ValidatorException $e) {
			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];
		}
	}

	/**
	 * 
	 */
	public function InscricaoDeAluno(array $data)
	{
		$inscricao = DB::table('alunos_jogos')
						->where('jogo_id', '=', $data['jogo_id'])
						->where('aluno_id', '=', $data['aluno_id'])
						->count();
		if($inscricao <= 0) {
			$inscricao = DB::table('alunos_jogos')
					->insert($data);
			return [
	            'error' => false,
	            'message' => 'A inscrição foi realizada com sucesso'
        	];
		} else {
			return [
	            'error' => true,
	            'message' => 'O aluno já está inscrito nesse jogo'
        	];
		}
	}

	/**
	 * 
	 */
	public function RemoverAlunoDoJogo(array $data)
	{
		$grupo = DB::table('alunos_jogos')
					->where('jogo_id', '=', $data['jogo_id'])
					->where('aluno_id', '=', $data['aluno_id'])
					->delete();
		if($grupo){
			return [
	            'error' => false,
	            'message' => 'O aluno foi retirado do grupo com sucesso'
        	];
        }

        return [
	            'error' => true,
	            'message' => 'erro ao retirar o aluno do grupo'
        ];
	}

}