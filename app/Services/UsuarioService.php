<?php 

namespace App\Services;

use App\Repositories\UsuarioRepository;
use App\Validators\UsuarioValidator;
use \Prettus\Validator\Exceptions\ValidatorException;

class UsuarioService
{

	private $repository;

	private $validator;

	public function __construct(UsuarioRepository $repository, UsuarioValidator $validator) 
	{
		$this->repository = $repository;
		$this->validator = $validator;
	}

	public function create(array $data) 
	{
		try{

			$this->validator->with($data)->passesOrFail();
			return $this->repository->create($data);

		}catch(ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}
	}

	public function update(array $data, $id) 
	{
		try{

			$this->validator->with($data)->passesOrFail();
			return $this->repository->update($data, $id);

		}catch(ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}	
	}

}