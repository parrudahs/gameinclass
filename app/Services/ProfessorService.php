<?php 

namespace App\Services;

use App\Repositories\ProfessorRepository;
use App\Repositories\UsuarioRepository;
use App\Validators\ProfessorValidator;
use \Prettus\Validator\Exceptions\ValidatorException;
use Prettus\Validator\Contracts\ValidatorInterface;

class ProfessorService
{

	private $repository;

	private $validator;

	private $usuario;

	public function __construct(ProfessorRepository $repository, ProfessorValidator $validator, UsuarioRepository $usuario) 
	{
		$this->repository = $repository;
		$this->validator = $validator;
		$this->usuario = $usuario;
	}

	public function create(array $data) 
	{
		try{

			$this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_CREATE );
			$usuario = $this->usuario->create($data);
			$data['usuario_id'] = $usuario->id;
			if( $this->repository->create($data))
				return [
					'error' => false,
					'message' => 'O Professor foi cadastrado com sucesso'
				];

		}catch(ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}
	}

	public function update(array $data, $id) 
	{
		try{

			$this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_UPDATE );
			$professor =  $this->repository->update($data, $id);
			return $this->usuario->update($data, $professor->usuario_id);

		} catch (ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}	
	}

}