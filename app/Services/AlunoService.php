<?php 

namespace App\Services;

use App\Repositories\AlunoRepository;
use App\Repositories\UsuarioRepository;
use App\Validators\AlunoValidator;
use \Prettus\Validator\Exceptions\ValidatorException;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\DB;

class AlunoService
{
	
	private $repository;

	private $usuario;

	private $validator;

	public function __construct(AlunoRepository $repository, AlunoValidator $validator, UsuarioRepository $usuario) 
	{
		$this->repository = $repository;
		$this->validator = $validator;
		$this->usuario = $usuario;
	}

	/**
	 * Cadastro de Aluno
	 */
	public function create(array $data) 
	{
		try{
			$this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_CREATE );
			$usuario = $this->usuario->create($data);
			$data['usuario_id'] = $usuario->id;
			if( $this->repository->create($data))
				return [
					'error' => false,
					'message' => 'Aluno cadastrado com sucesso'
				];

		} catch (ValidatorException $e) {
			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];
		}
	}

	/**
	 * Atualiza Aluno
	 */
	public function update(array $data, $id) 
	{
		try{

			$this->validator->with($data)->passesOrFail( ValidatorInterface::RULE_UPDATE );
			$aluno =  $this->repository->update($data, $id);
			return $this->usuario->update($data, $aluno->usuario_id);

		} catch (ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}	
	}

	public function AdicionarAlunoAoGrupo(array $data)
	{
		$grupo = DB::table('grupos_alunos')
						->where('grupo_id', '=', $data['grupo_id'])
						->where('aluno_id', '=', $data['aluno_id'])
						->count();
		if($grupo <= 0) {
			$grupo = DB::table('grupos_alunos')
					->insert($data);
			return [
	            'error' => false,
	            'message' => 'O aluno foi adicionado ao grupo com sucesso'
        	];
		} else {
			return [
	            'error' => true,
	            'message' => 'O aluno já faz parte do grupo'
        	];
		}
	}

	public function RemoverAlunoDoGrupo(array $data)
	{
		$grupo = DB::table('grupos_alunos')
					->where('grupo_id', '=', $data['grupo_id'])
					->where('aluno_id', '=', $data['aluno_id'])
					->delete();
		if($grupo){
			return [
	            'error' => false,
	            'message' => 'O aluno foi retirado do grupo com sucesso'
        	];
        }

        return [
	            'error' => true,
	            'message' => 'erro ao retirar o aluno do grupo'
        ];
	}

}

