<?php 

namespace App\Services;

use App\Repositories\RedeSocialRepository;
use App\Validators\RedeSocialValidator;
use \Prettus\Validator\Exceptions\ValidatorException;

class RedeSocialService
{

	private $repository;

	private $validator;

	public function __construct(RedeSocialRepository $repository, RedeSocialValidator $validator) 
	{
		$this->repository = $repository;
		$this->validator = $validator;
	}

	public function create(array $data) 
	{
		try{

			$this->validator->with($data)->passesOrFail();
			return $this->repository->create($data);

		} catch (ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}
	}

	public function update(array $data, $id) 
	{
		try{

			$this->validator->with($data)->passesOrFail();
			return $this->repository->update($data, $id);

		} catch (ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}	
	}

}