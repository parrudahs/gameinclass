<?php 

namespace App\Services;

use App\Repositories\MedalhaRepository;
use App\Validators\MedalhaValidator;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\DB;

class MedalhaService
{

	private $repository;

	private $validator;

	public function __construct(MedalhaRepository $repository, MedalhaValidator $validator) 
	{
		$this->repository = $repository;
		$this->validator = $validator;
	}

	public function create(array $data) 
	{
		try{

			$this->validator->with($data)->passesOrFail();
			return $this->repository->create($data);

		}catch(ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}
	}

	public function update(array $data, $id) 
	{
		try{

			$this->validator->with($data)->passesOrFail();
			return $this->repository->update($data, $id);

		} catch (ValidatorException $e) {

			return [
				'error' => true,
				'message' =>  $e->getMessageBag()
			];

		}	
	}

	public function AdicionarMedalhaAluno(array $data)
	{
		$grupo = DB::table('alunos_medalhas')
						->where('medalha_id', '=', $data['medalha_id'])
						->where('aluno_id', '=', $data['aluno_id'])
						->count();
		if($grupo <= 0) {
			$grupo = DB::table('grupos_alunos')
					->insert($data);
			return [
	            'error' => false,
	            'message' => 'A medalha foi adicionada com sucesso ao aluno.'
        	];
		} else {
			return [
	            'error' => true,
	            'message' => 'O aluno já ganhou essa medalha'
        	];
		}
	}

	public function RetirarMedalhaAluno(array $data)
	{
		$grupo = DB::table('alunos_medalhas')
					->where('medalha_id', '=', $data['medalha_id'])
					->where('aluno_id', '=', $data['aluno_id'])
					->delete();
		if($grupo){
			return [
	            'error' => false,
	            'message' => 'A medalha foi retirada com sucesso ao aluno.'
        	];
        }

        return [
	            'error' => true,
	            'message' => 'erro ao retirar o medalha do aluno'
        ];
	}

}