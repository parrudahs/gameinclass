<?php

namespace App\Http\Controllers;

use App\Entities\Alunos;
use App\Entities\Usuarios;
use App\Repositories\AlunoRepository;
use App\Repositories\UsuarioRepository;
use App\Services\AlunoService;
use App\Services\AutenticacaoService;
use GuzzleHttp\Client;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Requests;
use Illuminate\Http\Request;

class AlunoController extends Controller
{

    private $repository;
    private $usuario;
    private $autenticacao;
    private $service;

    /**
     * construtor 
     */
    public function __construct(AlunoRepository $repository, AlunoService $service, UsuarioRepository $usuario, AutenticacaoService $autenticacao)
    {
    	$this->repository = $repository;
    	$this->service = $service;
        $this->usuario = $usuario;
        $this->autenticacao = $autenticacao;
    }

    /**
     * retorna todos os alunos
     */
    public function index()
    {
    	return $this->repository->with(['usuario'])->all();
    }

    /**
     * cria um novo aluno
     */
    public function store(Request $request)
    {
    	return $this->service->create($request->all());
    }

    /**
     * retorna um aluno, pelo ID
     */
    public function show($id)
    {
    	try {
            return [
                'error' => false,
                'data' => $this->repository->with(['usuario'])->find($id)
            ];
    	} catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O aluno não foi encontrado'
            ];
        }
    }

    /**
     * atualiza o aluno pelo ID
     */
    public function update(Request $request, $id)
    {
        try {
           	$this->service->update($request->all(), $id);
            return [
                'error' => false,
                'data' => $this->repository->with(['usuario'])->find($id),
                'message' => 'O aluno foi atualizado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O aluno não foi encontrado'
            ];
        }
    }

    /**
     *  deleta uma aluno
     */
    public function destroy($id)
    {
        try {
            $this->repository->find($id)->delete();
            return [
                'error' => false,
                'message' => 'O aluno foi deletado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O aluno não foi encontrado'
            ];
        }
    	
    }

    /**
     * Faz a autenticação do aluno
     * @param Request $request
     * @return array
     */
    public function getLoginAluno(Request $request)
    {
        return $this->autenticacao->getLoginAluno($request->all());
    }

    /**
     * adiciona um aluno ao grupo
     */
    public function AdicionarAlunoAoGrupo(Request $request)
    {
        return $this->service->AdicionarAlunoAoGrupo($request->all());
    }

    /**
     * remove um aluno do grupo
     */
    public function RemoverAlunoDoGrupo(Request $request) 
    {
        return $this->service->RemoverAlunoDoGrupo($request->all());
    }


}
