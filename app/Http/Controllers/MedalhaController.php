<?php

namespace App\Http\Controllers;

use App\Entities\Medalhas;
use App\Repositories\MedalhaRepository;
use App\Services\MedalhaService;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Requests;
use Illuminate\Http\Request;

class MedalhaController extends Controller
{

    private $repository;

    private $service;

    /**
     * construtor
     */
    public function __construct(MedalhaRepository $repository, MedalhaService $service) 
    {
    	$this->repository = $repository;
    	$this->service = $service;
    }

    /**
     * retorna todas as medalhas 
     */
    public function index()
    {
    	return $this->repository->with(['professor'])->all();
    }

    /**
     * cria uma nova medalha
     */
    public function store(Request $request)
    {
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            if($file->isValid()) {
                $destinationPath = 'uploads';
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renameing image
                $file->move($destinationPath, $fileName); // uploading file to given path

                $request['imagem'] = $fileName;

                return $this->service->create($request->all());
            }
        }
        return [
            'error' => true,
            'message' => 'erro ao adicionar medalha' 
        ];
    }

    /**
     * retorna uma medalha pelo ID
     */
    public function show($id)
    {
    	try {
            return [
                'error' => false,
                'data' => $this->repository->with(['professor'])->find($id)
            ];
    	} catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'A medalha não foi encontrada'
            ];
        }
    }

    /**
     * atualiza um medalha pelo ID
     */
    public function update(Request $request, $id)
    {
        try {
           	$this->service->update($request->all(), $id);
            return [
                'error' => false,
                'data' => $this->show($id),
                'message' => 'A medalha foi atualizado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'A medalha não foi encontrada'
            ];
        }
    }

    /**
     * deleta uma medalha pelo ID
     */
    public function destroy($id)
    {
        try {
            $this->repository->find($id)->delete();
            // falta deleta a imagem do servidor
            return [
                'error' => false,
                'message' => 'A medalha foi deletado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'A medalha não foi encontrada'
            ];
        }
    	
    }

    /**
     * adiciona uma medalha ao aluno
     */
    public function AdicionarMedalhaAluno(Request $request)
    {
        return $this->service->AdicionarMedalhaAluno($request->all());
    }

    /**
     * retira uma medalha do aluno
     */
    public function RetirarMedalhaAluno(Request $request)
    {
        return $this->service->RetirarMedalhaAluno($request->all());
    }
}
