<?php

namespace App\Http\Controllers;

use App\Entities\Usuarios;
use App\Repositories\UsuarioRepository;
use App\Services\UsuarioService;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\UsuarioController;

class UsuarioController extends Controller
{
    private $repository;

    private $service;

    /**
     * 
     */
    public function __construct(UsuarioRepository $repository, UsuarioService $service) 
    {
    	$this->repository = $repository;
    	$this->service = $service;
    }

    /**
     * 
     */
    public function index()
    {
    	return $this->repository->all();
    }

    /**
     * 
     */
    public function store(Request $request)
    {
    	return $this->service->create($request->all());
    }

    /**
     * 
     */
    public function show($id) 
    {
        try {
            return [
                'error' => false,
                'data' => $this->repository->find($id)
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O usuario não foi encontrado'
            ];
        }
    }

    /**
     * 
     */
    public function update(Request $request, $id)
    {
        try {
           	$this->service->update($request->all(), $id);
            return [
                'error' => false,
                'data' => $this->repository->find($id),
                'message' => 'O usuario foi atualizado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O usuario não foi encontrado'
            ];
        }
    }

    /**
     * 
     */
    public function destroy($id)
    {
        try {
            $this->repository->find($id)->delete();
            return [
                'error' => false,
                'message' => 'O usuario foi deletado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O usuario não foi encontrado'
            ];
        }    	
    }


    
}
