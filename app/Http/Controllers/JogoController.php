<?php

namespace App\Http\Controllers;

use App\Entities\Jogos;
use App\Repositories\JogoRepository;
use App\Repositories\RankingRepository;
use App\Repositories\UsuarioRepository;

use App\Services\JogoService;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Requests;
use Illuminate\Http\Request;

class JogoController extends Controller
{

    private $repository;

    private $service;

    private $usuario;

    private $ranking;

    /**
     * construtor
     */
    public function __construct(JogoRepository $repository, JogoService $service, RankingRepository $ranking, UsuarioRepository $usuario) 
    {
    	$this->repository = $repository;
    	$this->service = $service;
        $this->ranking = $ranking;
        $this->usuario = $usuario;
    }

    /**
     * retorna todos os jogos
     */
    public function index()
    {
    	return $this->repository->all();
    }

    /**
     * cria uma novo jogo
     */
    public function store(Request $request)
    {
    	return $this->service->create($request->all());
    }

    /**
     * retorna um jogo pelo ID, com os alunos que fazem parte dele e o professor
     */
    public function show($id)
    {
    	try {

            $dados = $this->repository->with(['alunos','professor','ranking','sessoes'])->find($id);
            
            $alunos = array();

            $id_professor = $dados['professor']['usuario_id'];
            $professor =  $this->usuario->findWhere(['id' => $id_professor]);

            unset($dados['professor']);

            $i = 0;
            
            foreach ($dados['alunos'] as $d) {
                $id_aluno = $d['usuario_id'];
                $alunos[] = $this->usuario->findWhere(['id' => $id_aluno]);
                $i++;
            }
            unset($dados['alunos']);

            return [
                'error' => false,
                'data' => [
                            $dados,
                            'alunos' => $alunos,
                            'professor' => $professor
                          ],
            ];

    	} catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O Jogo não foi encontrado'
            ];
        }
    }

    /**
     * atualiza um jogo
     */
    public function update(Request $request, $id)
    {
        try {
           	$this->service->update($request->all(), $id);
            return [
                'error' => false,
                'data' => $this->repository->with(['professor','ranking'])->find($id),
                'message' => 'O jogo foi atualizado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O jogo não foi encontrado'
            ];
        } catch (\ErrorException $e){
            return [
                'error' => true,
                'message' => 'O jogo não foi encontrado'
            ];
        }
    }

    /**
     * deleta um jogo
     */
    public function destroy($id)
    {
        try {
            $rank = $this->ranking->findByField('jogo_id', $id)->first();
            $this->ranking->find($rank->id)->delete();
            $this->repository->find($id)->delete();
            return [
                'error' => false,
                'message' => 'O jogo foi deletado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O jogo não foi encontrado'
            ];
        } catch (\ErrorException $e){
            return [
                'error' => true,
                'message' => 'O jogo não foi encontrado'
            ];
        }
    	
    }

    /**
     * adiciona um aluno ao jogo 
     */
    public function InscricaoDeAluno(Request $request)
    {
       return $this->service->InscricaoDeAluno($request->all());
    }

    /**
     * remove um aluno do jogo
     */
    public function RemoverAlunoDoJogo(Request $request) 
    {
        return $this->service->RemoverAlunoDoJogo($request->all());
    }

}
