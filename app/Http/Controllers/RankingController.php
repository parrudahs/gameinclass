<?php

namespace App\Http\Controllers;

use App\Entities\Ranking;
use App\Repositories\RankingRepository;
use App\Services\RankingService;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Requests;
use Illuminate\Http\Request;

class RankingController extends Controller
{

    private $repository;

    private $service;

    /**
     * construtor
     */
    public function __construct(RankingRepository $repository, RankingService $service) 
    {
    	$this->repository = $repository;
    	$this->service = $service;
    }

    /**
     * retorna todos os rankings
     */
    public function index()
    {
    	return $this->repository->all();
    }

    /**
     * cria uma novo ranking
     */
    public function store(Request $request)
    {
    	return $this->service->create($request->all());
    }

    /**
     * mostra apenas um ranking
     */
    public function show($id)
    {
    	try {
            return [
                'error' => false,
                'data' => $this->repository->with(['usuario'])->find($id)
            ];
    	} catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O ranking não foi encontrado'
            ];
        }
    }

    /**
     * atualiza um ranking pelo ID
     */
    public function update(Request $request, $id)
    {
        try {
           	$this->service->update($request->all(), $id);
            return [
                'error' => false,
                'data' => $this->repository->with(['usuario'])->find($id),
                'message' => 'O ranking foi atualizado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O ranking não foi encontrado'
            ];
        }
    }

    /**
     * deleta um raking pelo ID
     */
    public function destroy($id)
    {
        try {
            $this->repository->find($id)->delete();
            return [
                'error' => false,
                'message' => 'O ranking foi deletado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O ranking não foi encontrado'
            ];
        }
    	
    }

}
