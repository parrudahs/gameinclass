<?php

namespace App\Http\Controllers;

use App\Entities\Grupos;
use App\Repositories\GrupoRepository;
use App\Services\GrupoService;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Requests;
use Illuminate\Http\Request;

class GrupoController extends Controller
{

    private $repository;

    private $service;

    /**
     * construtor
     */
    public function __construct(GrupoRepository $repository, GrupoService $service) 
    {
    	$this->repository = $repository;
    	$this->service = $service;
    }

    /**
     * retorna todos os grupos
     */
    public function index()
    {
    	return $this->repository->all();
    }

    /**
     * cria um novo grupo
     */
    public function store(Request $request)
    {
    	return $this->service->create($request->all());
    }

    /**
     * retorna um grupo pelo ID
     */
    public function show($id)
    {
    	try {
            return [
                'error' => false,
                'data' => $this->repository->with(['alunos', 'professor'])->find($id)
            ];
    	} catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O grupo não foi encontrado'
            ];
        }
    }

    /**
     * atualiza um grupo pelo ID
     */
    public function update(Request $request, $id)
    {
        try {
           	$this->service->update($request->all(), $id);
            return [
                'error' => false,
                'data' => $this->repository->with(['alunos', 'professor'])->find($id),
                'message' => 'O grupo foi atualizado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O grupo não foi encontrado'
            ];
        }
    }

    /**
     * deleta um aluno pelo ID
     */
    public function destroy($id)
    {
        try {
            $this->repository->find($id)->delete();
            return [
                'error' => false,
                'message' => 'O grupo foi deletado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O grupo não foi encontrado'
            ];
        }
    	
    }

}