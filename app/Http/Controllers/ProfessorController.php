<?php

namespace App\Http\Controllers;

use App\Entities\Professores;
use App\Repositories\ProfessorRepository;
use App\Services\ProfessorService;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\AutenticacaoService;
use App\Http\Requests;
use Illuminate\Http\Request;

class ProfessorController extends Controller
{

    private $repository;

    private $service;

    private $autenticacao;

    /**
     * construtor
     */
    public function __construct(ProfessorRepository $repository, ProfessorService $service,  AutenticacaoService $autenticacao) 
    {
    	$this->repository = $repository;
        $this->autenticacao = $autenticacao;
    	$this->service = $service;
    }

    /**
     * retorna todos os professores  
     */
    public function index()
    {
    	return $this->repository->with(['usuario'])->all();
    }

    /**
     * cria um novo professor
     */
    public function store(Request $request)
    {
    	return $this->service->create($request->all());
    }

    /**
     * retorna um professor pelo ID
     */
    public function show($id)
    {
    	try {
            return [
                'error' => false,
                'data' => $this->repository->with(['usuario', 'medalhas'])->find($id)
            ];
    	} catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O professor não foi encontrado'
            ];
        }
    }

    /**
     * Atualiza um professor pelo ID
     */
    public function update(Request $request, $id)
    {
        try {
           	$this->service->update($request->all(), $id);
            return [
                'error' => false,
                'data' => $this->repository->with(['usuario'])->find($id),
                'message' => 'O professor foi atualizado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O professor não foi encontrado'
            ];
        }
    }

    /**
     * deleta um professor
     */
    public function destroy($id)
    {
        try {
            $this->repository->find($id)->delete();
            return [
                'error' => false,
                'message' => 'O professor foi deletado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'O professor não foi encontrado'
            ];
        }
    	
    }

    /**
     * faz a autenticação do professor
     */
    public function getLoginProfessor(Request $request)
    {
        return $this->autenticacao->getLoginProfessor($request->all());
    }

}
