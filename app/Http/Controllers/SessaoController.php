<?php

namespace App\Http\Controllers;

use App\Entities\Sessoes;
use App\Repositories\SessaoRepository;
use App\Services\SessaoService;
use App\Repositories\JogoRepository;
use App\Repositories\UsuarioRepository;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Requests;
use Illuminate\Http\Request;

class SessaoController extends Controller
{

    private $repository;

    private $service;

    private $usuario;

    private $jogo;

    /**
     * construtor
     */
    public function __construct(SessaoRepository $repository, SessaoService $service, JogoRepository $jogo, UsuarioRepository $usuario) 
    {
    	$this->repository = $repository;
    	$this->service = $service;
        $this->jogo = $jogo;
        $this->usuario = $usuario;
    }

    /**
     * retorna todas as sessões
     */
    public function index()
    {
    	return $this->repository->all();
    }

    /**
     * cria uma nova sessão 
     */
    public function store(Request $request)
    {
    	return $this->service->create($request->all());
    }

    /**
     * retorna uma sessão pelo ID, com o jogo que ele faz parte e os alunos dessa sessão 
     */
    public function show($id)
    {
    	try {
            $dados = $this->repository->with(['jogo', 'alunos'])->find($id);

            $i = 0;
            $alunos = array();

            foreach ($dados['alunos'] as $d) {
                $id_aluno = $d['usuario_id'];
                $alunos[] = $this->usuario->findWhere(['id' => $id_aluno]);
                $i++;
            }

            unset($dados['alunos']);

            return [
                'error' => false,
                'data' => [
                            $dados,
                            'alunos' => $alunos,
                           ]
            ];
    	} catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'A sessão não foi encontrada'
            ];
        }
    }

    /**
     * atualiza uma sessão
     */
    public function update(Request $request, $id)
    {
        try {
           	$this->service->update($request->all(), $id);
            return [
                'error' => false,
                'data' => $this->repository->find($id),
                'message' => 'A sessão foi atualizado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'A sessão não foi encontrada'
            ];
        }
    }

    /**
     * retorna todos os jogos de um professor
     */
    public function JogosDoProfessor($id) 
    {
        try {
            $jogos = $this->jogo->findByField('professor_id', $id); 
            if(!count($jogos)) 
                return [
                    'error' => true,
                    'message' => 'Nenhuma jogo do professor foi encontrada'
                ];               
            return $jogos;
            
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'Nenhuma jogo do professor foi encontrada'
            ];
        }
    }

    /**
     * deleta uma sessão pelo ID
     */
    public function destroy($id)
    {
        try {
            $this->repository->find($id)->delete();
            return [
                'error' => false,
                'message' => 'A sessão foi deletado com sucesso'
            ];
        } catch (ModelNotFoundException $e) {
            return [
                'error' => true,
                'message' => 'A sessão não foi encontrada'
            ];
        }
    	
    }

    /**
     * adiciona presença a um aluno
     */
    public function AdicionarPresencaDoAlunoNaSessao(Request $request) 
    {
        return $this->service->AdicionarPresencaDoAlunoNaSessao($request->all());
    }

    /**
     * retira presença de um aluno
     */
    public function RetirarPresencaDoAluno(Request $request) 
    {
        return $this->service->RetirarPresencaDoAluno($request->all());
    }

}
