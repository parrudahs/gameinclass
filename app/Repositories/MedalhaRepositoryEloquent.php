<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MedalhaRepository;
use App\Entities\Medalhas;
use App\Validators\MedalhaValidator;

/**
 * Class MedalhaRepositoryEloquent
 * @package namespace App\Repositories;
 */
class MedalhaRepositoryEloquent extends BaseRepository implements MedalhaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Medalhas::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
