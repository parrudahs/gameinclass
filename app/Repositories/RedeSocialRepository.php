<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RedeSocialRepository
 * @package namespace App\Repositories;
 */
interface RedeSocialRepository extends RepositoryInterface
{
    //
}
