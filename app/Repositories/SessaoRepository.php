<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SessaoRepository
 * @package namespace App\Repositories;
 */
interface SessaoRepository extends RepositoryInterface
{
    //
}
