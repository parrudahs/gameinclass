<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RankingRepository;
use App\Entities\Ranking;
use App\Validators\RankingValidator;

/**
 * Class RankingRepositoryEloquent
 * @package namespace App\Repositories;
 */
class RankingRepositoryEloquent extends BaseRepository implements RankingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Ranking::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
