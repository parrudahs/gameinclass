<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface GrupoRepository
 * @package namespace App\Repositories;
 */
interface GrupoRepository extends RepositoryInterface
{
    //
}
