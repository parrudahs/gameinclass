<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RedeSocialRepository;
use App\Entities\RedesSociais;
use App\Validators\RedeSocialValidator;

/**
 * Class RedeSocialRepositoryEloquent
 * @package namespace App\Repositories;
 */
class RedeSocialRepositoryEloquent extends BaseRepository implements RedeSocialRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RedesSociais::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
