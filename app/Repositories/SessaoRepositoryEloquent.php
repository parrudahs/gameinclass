<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SessaoRepository;
use App\Entities\Sessoes;
use App\Validators\SessaoValidator;

/**
 * Class SessaoRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SessaoRepositoryEloquent extends BaseRepository implements SessaoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Sessoes::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
