<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\GrupoRepository;
use App\Entities\Grupos;
use App\Validators\GrupoValidator;

/**
 * Class GrupoRepositoryEloquent
 * @package namespace App\Repositories;
 */
class GrupoRepositoryEloquent extends BaseRepository implements GrupoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Grupos::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
