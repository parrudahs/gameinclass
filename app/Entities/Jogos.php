<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Jogos extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
    	'nome', 
    	'descricao', 
    	'inscricao', 
    	'curso', 
    	'semestre', 
    	'sessao_label', 
    	'professor_id'
    ];

    /**
     * 
     */
    public function professor() 
    {
    	return $this->belongsTo(Professores::class);
    }

    public function ranking()
    {
        return $this->belongsTo(Ranking::class);
    }

    /**
     * 
     */
    public function sessoes() 
    {
    	return $this->hasMany(Sessoes::class, 'jogo_id');
    }

    /**
     * 
     */
    public function alunos()
    {
        return $this->belongsToMany(Alunos::class, 'alunos_jogos', 'jogo_id', 'aluno_id');
    }


    /**
     * 
     */
    public function medalhas()
    {
        return $this->belongsToMany(Medalhas::class, 'jogos_medalhas', 'jogo_id', 'medalha_id');
    }


}
