<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Grupos extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
    	 'nome',
         'professor_id'
    ];

    /**
     * 
     */
    public function professor() 
    {
    	return $this->belongsTo(Professores::class);
    }

    /**
     * 
     */
    public function alunos()
    {
    	return $this->belongsToMany(Alunos::class, 'grupos_alunos', 'grupo_id', 'aluno_id');
    }
    
}
