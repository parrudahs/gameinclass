<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Medalhas extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
    	'nome',
        'descricao',
        'imagem',
        'professor_id'
    ];

    /**
     * 
     */
    public function alunos()
    {
    	return $this->belongsToMany(Alunos::class, 'alunos_medalhas', 'medalha_id', 'aluno_id');
    }

    /**
     * 
     */
    public function jogos()
    {
        return $this->belongsToMany(Jogos::class, 'jogos_medalhas', 'medalha_id', 'jogo_id');
    }

    /**
     * 
     */
    public function sessoes()
    {
        return $this->belongsToMany(Sessoes::class, 'sessoes_medalhas', 'medalha_id', 'sessao_id');
    }

    public function professor()
    {
        return $this->belongsTo(Professores::class);
    }

}
