<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Alunos extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['rga', 'usuario_id'];

    /**
     * 
     */
    public function usuario() 
    {
    	return $this->belongsTo(Usuarios::class);
    }

    /**
     * 
     */
    public function jogos()
    {
    	return $this->belongsToMany(Jogos::class, 'alunos_jogos', 'aluno_id', 'jogo_id');
    }

    /**
     * 
     */
    public function grupos()
    {
    	return $this->belongsToMany(Grupos::class, 'grupos_alunos', 'aluno_id', 'grupo_id');
    }

    /**
     * 
     */
    public function medalhas()
    {
    	return $this->belongsToMany(Medalhas::class, 'alunos_medalhas', 'aluno_id', 'medalha_id');
    }

    /**
     * 
     */
    public function sessoes()
    {
    	return $this->belongsToMany(Sessoes::class, 'sessoes_alunos', 'aluno_id', 'sessao_id');
    }

}
