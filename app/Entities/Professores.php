<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Professores extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['cpf','administrador', 'usuario_id'];

    public function usuario() {
    	return $this->belongsTo(Usuarios::class);
    }

    public function jogos() {
    	return $this->hasMany(Jogos::class);
    }

    public function grupos() {
    	return $this->hasMany(Grupos::class);
    }

    public function medalhas() {
        return $this->hasMany(Medalhas::class, 'professor_id');
    }
        
}