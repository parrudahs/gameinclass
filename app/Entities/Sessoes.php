<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Sessoes extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
    	'titulo',
    	'sessao_observacao',
    	'sessao_tema',
    	'presencial',
    	'inicio',
    	'termino',
    	'sequencia',
    	'jogo_id'
    ];
    
    /**
     * 
     */
    public function jogo() {
    	return $this->belongsTo(Jogos::class);
    }

    /**
     * 
     */
    public function alunos()
    {
        return $this->belongsToMany(Alunos::class, 'sessoes_alunos', 'sessao_id', 'aluno_id');
    }

    /**
     * 
     */
    public function medalhas()
    {
        return $this->belongsToMany(Medalhas::class, 'sessoes_medalhas', 'sessao_id', 'medalha_id');
    }

}
