<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

use Illuminate\Contracts\Encryption\DecryptException;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Usuarios extends Authenticatable implements Transformable
{
    use HasApiTokens, TransformableTrait, Notifiable;

	//protected $table = 'usuarios';

    protected $fillable = ['nome', 'email', 'sobrenome', 'senha'];

    public function setSenhaAttribute($senha)
    {
        $this->attributes['senha'] = encrypt($senha);
    }

    public function getSenhaAttribute($senha)
    {
        try {
            return decrypt($senha);
        } catch (DecryptException $e) {
            return $e;
        }        
    }

    public function alunos()
    {
    	return $this->hasMany(Alunos::class);
    }

    public function professores()
    {
    	return $this->hasMany(Professores::class);
    }    
}
