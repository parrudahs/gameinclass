<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Ranking extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
    	'pontos_individual',
        'pontos_grupo',
        'medalhas_individual',
        'medalhas_grupo',
        'jogo_id'
    ];

    public function jogo() 
    {
    	return $this->hasOne(Jogos::class);
    }
    
}
