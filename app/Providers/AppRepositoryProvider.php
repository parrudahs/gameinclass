<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\UsuarioRepository::class, 
            \App\Repositories\UsuarioRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\AlunoRepository::class, 
            \App\Repositories\AlunoRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\ProfessorRepository::class, 
            \App\Repositories\ProfessorRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\JogoRepository::class, 
            \App\Repositories\JogoRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\SessaoRepository::class, 
            \App\Repositories\SessaoRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\MedalhaRepository::class, 
            \App\Repositories\MedalhaRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\GrupoRepository::class, 
            \App\Repositories\GrupoRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\RankingRepository::class, 
            \App\Repositories\RankingRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\RedeSocialRepository::class, 
            \App\Repositories\RedeSocialRepositoryEloquent::class
        );
    }
}
