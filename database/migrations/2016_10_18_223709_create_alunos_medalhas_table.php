<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunosMedalhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunos_medalhas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medalha_id')->unsigned();
            $table->integer('aluno_id')->unsigned();
            $table->foreign('medalha_id')->references('id')->on('medalhas')->onDelete('cascade');      
            $table->foreign('aluno_id')->references('id')->on('alunos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunos_medalhas');
    }
}
