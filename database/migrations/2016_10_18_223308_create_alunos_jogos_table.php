<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunosJogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunos_jogos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jogo_id')->unsigned();
            $table->integer('aluno_id')->unsigned();
            $table->foreign('jogo_id')->references('id')->on('jogos')->onDelete('cascade');    
            $table->foreign('aluno_id')->references('id')->on('alunos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunos_jogos');
    }
}
