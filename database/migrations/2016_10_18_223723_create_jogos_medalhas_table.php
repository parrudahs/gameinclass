<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJogosMedalhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jogos_medalhas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medalha_id')->unsigned();
            $table->integer('jogo_id')->unsigned();
            $table->foreign('medalha_id')->references('id')->on('medalhas')->onDelete('cascade');        
            $table->foreign('jogo_id')->references('id')->on('jogos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jogos_medalhas');
    }
}
