<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessoesMedalhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessoes_medalhas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medalha_id')->unsigned();
            $table->integer('sessao_id')->unsigned();
            $table->foreign('medalha_id')->references('id')->on('medalhas')->onDelete('cascade');    
            $table->foreign('sessao_id')->references('id')->on('sessoes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessoes_medalhas');
    }
}
