<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rankings', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('pontos_individual');
            $table->boolean('pontos_grupo');
            $table->boolean('medalhas_individual');
            $table->boolean('medalhas_grupo');
            $table->integer('jogo_id')->unsigned();
            $table->foreign('jogo_id')->references('id')->on('jogos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rankings');
    }
}
