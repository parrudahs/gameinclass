<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedalhasGruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medalhas_grupos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('medalha_id')->unsigned();
            $table->integer('grupo_id')->unsigned();
            $table->foreign('medalha_id')->references('id')->on('medalhas')->onDelete('cascade');        
            $table->foreign('grupo_id')->references('id')->on('grupos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medalhas_grupos');
    }
}
